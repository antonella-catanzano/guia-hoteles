module.exports = function (grunt) {
    // Herramienta grunt para comilar archivos sass
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true, 
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: 'css',
                    ext: '.css'
                }]
            }
        },
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },

        browserSync: {
            dev: {
                bsFiles: { //browser files
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' // directorio base para nuestro servidor
                    }
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');  
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.registerTask('css', ['sass']);   
};